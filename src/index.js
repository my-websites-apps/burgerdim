
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Applic from './Applic';
import Context from "./context/context";



ReactDOM.render(
<React.StrictMode>
  <Context>
  <Applic />
  </Context> 
</React.StrictMode>,
document.querySelector('.output')
);