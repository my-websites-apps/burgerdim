import React, {useState, useEffect} from "react";
import {CartState} from "../../context/context";
import {ListGroup, Row, Col, Form, Image, Button} from "react-bootstrap";
import { MdDelete} from "react-icons/md";
import {NavLink} from "../navbar/navigbarelem";
import "./cart.css";
import StarRating from "../menu/star_rating/star_rating";



function CartItems() {
    const {
        state:{cart}, dispatch
      } = CartState();

      const [total, setTotal] = useState();

      useEffect(() =>{
        setTotal(cart.reduce((acc,curr)=> acc+ Number(curr.price)*curr.qty, 0))

      }, [cart]
      );
    
    


    return ( 
      <>
      <div className ="header_block"> 
            <h1 className = "header_1">Cart</h1>
            </div>
        <div className= "cart">
          
            
            <div className="product_container">
                <ListGroup >
                { cart.map((pr) =>
                <ListGroup.Item key={pr.id}>
                <Row className="mb-3">
                  <Col xs={4} sm={4} md ={4} lg={2}><span ><Image src={pr.image} alt={pr.name} fluid rounded width={100} height={100} ></Image></span>
                  </Col>
                  <Col xs={6} sm={6} md ={6} lg={3}><span >{pr.title}</span>
                  </Col>
                      
                  <Col xs={1} sm={1} md ={1} lg = {1}><span> ${pr.price}</span></Col>
                  <Col xs={6} sm={6} md ={6} lg="auto" xl={3}>
                        <StarRating rating={pr.rate}/>
                  </Col>
                  <Col xs={3} sm={3} md ={3} lg="auto" >
                        <Form.Control as="select" 
                              value={pr.qty} 
                              onChange={(e)=> dispatch({
                              type: "change_pr_qty_in_cart",
                              payload: {
                                  id:pr.id,
                                  qty:e.target.value,
                              },
                              }) }>
                                  {[...Array(Number(pr.inStock)).keys()].map((x)=>(<option key={x+1}>{x+1}</option>))}
                        </Form.Control>
                  </Col>
                  <Col xs={3} sm={3}  md ={3} lg="auto">
                    <Button
                      type="button"
                      variant="light"
                        
                      onClick={()=>
                          dispatch({
                            type:"remove_from_cart",
                            payload:pr,
                          })}
                      >
                        <MdDelete 
                          fontSize="20px"
                          style={{ cursor:"pointer"}}/>
                    </Button>
                           
                  </Col>
                
                </Row>
                    
                        
                </ListGroup.Item>
                )}
                </ListGroup>
                </div>
            <div className="filters_summary">
                <div>
                  <div className="title"> Subtotal ({cart.length}) items</div>
                  <div> Total: ${total}</div>
                </div>
                <button className="to_checkout" disabled={cart.length === 0}>
                  <NavLink to='/checkout' activestyle="true"  exact="true" style={{color:"#fff"}}> Proceed to Checkout</NavLink>
                </button>
            </div>
        </div>
        </>
    )}

    export default CartItems
