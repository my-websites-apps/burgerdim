import React, {useState, useRef} from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faTwitter,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import  { MdClose} from "react-icons/md";
import { GiHamburger } from "react-icons/gi";
import "./contacts.css";
import "../authentification/account.css";
import "../effects/header/header.css";
import validateFormContacts from '../effects/form_validation/form_validation_contacts';
import ModalWindow from '../modal/modal';



const socialMediaHandlesLinks = {

    instagram: 'https://www.instagram.com',
    facebook: 'https://www.facebook.com',
    twitter: 'https://twitter.com'
  }
  
  
  function Iframe(props) {
    return (<div className='map-responsive'  dangerouslySetInnerHTML={ {__html:  props.iframe?props.iframe:""}} />);
  }

  let iframe =  '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d1695.5248804340222!2d30.526614509187418!3d50.451961079928566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e3!4m0!4m5!1s0x40d4ce51c3bdb9a1%3A0xff877737cf946b28!2z0YPQuy4g0JrRgNC10YnQsNGC0LjQuiwgMSwg0JrQuNC10LIsINCj0LrRgNCw0LjQvdCwLCAwMjAwMA!3m2!1d50.4514297!2d30.527494599999997!5e0!3m2!1sru!2sca!4v1680292800281!5m2!1sru!2sca" width="600" height="600" style="border:0; width:100%; height:100%; position:relative; top:0; left:0" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';




   
/*Form validation*/

function Contacts() {
    const [modalActive, setModalActive] = useState(false);
    /*Form validation*/
    const formRef = useRef(null);

    const [form, setForm] = useState ({
      email:"",
      fullName:"",
      message:"",

    })
  
    const [errors, setErrors] = useState({})
  
  
    const handleChange = (event) => {
        setForm ({
            ...form,
            [event.target.name]: event.target.value,
            
        })
  
        if (errors[event.target.name])
        setErrors ({
            ...errors,
            [event.target.name]:null
        })
    
      }
  
    
    const [validated, setValidated] = useState(false);
  
    const formErrors = validateFormContacts(form);

    const handleSubmit = (e) => {
      
        if(e) e.preventDefault();
  
        if ( Object.keys(formErrors).length > 0)
        { setValidated(false);
            setErrors(formErrors);
            console.log(formErrors)
            console.log("form is not submitted");

        }  else {
        setValidated(true);
        setModalActive(true);
        console.log("form submitted");
        
        }
        
        console.log(validated)
      }


      const handleKeyDown = (ev)=>{
        if(ev.keyCode ===13){ // enter button
            ev.preventDefault();
         formRef.current.submit()
        }
     }


    return (  
    <>  <div>
            <div className ="header_block"> 
                <h1 className = "header_1">Contact Us</h1>
            </div>
            
        </div>
        <div className="contact_block">
                    
            <div className="message">
                <h2>Get in touch with Us and send<br/> a Message </h2>
                          
                <form action="./index.php" ref={formRef} noValidate validated={validated.toString()} onSubmit={handleSubmit} >
                    <div className="form"  onKeyDown={handleKeyDown}>
                                <input 
                                type="text" 
                                id="one" 
                                placeholder="Name"
                                name="fullName"
                                value={form.fullName}
                                style={errors.fullName ? { border: "2px solid red" } : null}
                                onChange={handleChange}
                                required/>

                                <input 
                                type="email" 
                                id="two" 
                                placeholder="Email"
                                name="email"
                                value={form.email}
                                style={errors.email ? { border: "2px solid red" } : null}
                                 onChange={handleChange}
                                />
                                <textarea 
                                id="three" 
                                rows="2" 
                                name="message" 
                                placeholder="Message" 
                                value={form.message}
                                style={errors.message ? { border: "2px solid red" } : null}
                                onChange={handleChange}
                                required/>

                                <button type="submit" className="send_message" disabled={validated===true} >
                                Send
                                </button>
                    </div> 
                </form>
                    
                
                    
            </div>
            <div className="map"> <Iframe iframe={iframe} /></div>
        </div> 
        <div className="footer_2">
        
                <div>
                    <p>
                        <span >Email:</span>  <a href="mailto:kirilsveta@gmail.com">BurgerDim@gmail.com</a><br/>
                        <span>Phone:</span>  <a href="tel:+111111111111">+(111) 111111111</a><br/>
                        <span>Headquarters:</span> Khreshchatyk St, 1, 3rd Floor,<br/>
                        Kyiv,<br/>
                        Ukraine<br/>
                    </p>
                </div>
                <div>
                    <h5>Opening Hours</h5>
                    <p>Monday - Friday : 10:00 am - 05:00 pm<br/>Saturday : 10:00 am - 02:00 pm</p>
                </div>
                <div>
                    <h6>Connect With Us On</h6>
                    <div className="social-menu">
                        <a  href={socialMediaHandlesLinks.facebook} ><p>Facebook</p>
                        <FontAwesomeIcon icon={faFacebook} size="2x" /> 
                        </a>
                        <a  href={socialMediaHandlesLinks.instagram}><p>Instagram</p>
                        <FontAwesomeIcon icon={faInstagram} size="2x" />
                        </a>
                        <a href={socialMediaHandlesLinks.twitter}> <p>Twitter</p>
                        <FontAwesomeIcon icon={faTwitter}  size="2x" />
                         </a>
                    </div>                  
                </div>
        </div>
        <ModalWindow active={modalActive}  setActive={setModalActive} >
            <div className="modal_wind_content_login">
              <h2 >BurgerDim</h2>
              <GiHamburger fontSize={"100px"}/>
                <p style={{fontSize:"1.8em"}}>Hello,<span >&nbsp;</span>{form.fullName}!<br/>Thank you for contacting us!</p>
                <MdClose className="close" onClick={()=> setModalActive(false)}></MdClose> 
            </div>
           
        </ModalWindow>

    </>  )
    
}

export default Contacts