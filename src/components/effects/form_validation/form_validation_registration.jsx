
export default  function validateFormRegistration (form) {
      
     
    let newErrors = {}

    //full name
    if (!form.fullName || form.fullName ==="") newErrors.fullName = "Please write your full name";

    //email
     if (!form.email || form.email ==="") newErrors.email = "Email address is required"
    else if (!/\S+@\S+\.\S+/.test(form.email))  newErrors.email = "Please provide a valid email address";

    //phone
    if (!form.phone || form.phone ==="") newErrors.phone = "Phone number is required"
    else if (! /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/.test(form.phone))  
    newErrors.phone = "Please provide a valid phone number";

 

    //password
    const uppercaseRegExp   = /(?=.*?[A-Z])/;
    const lowercaseRegExp   = /(?=.*?[a-z])/;
    const digitsRegExp      = /(?=.*?[0-9])/;
    const specialCharRegExp = /(?=.*?[#?!@$%^&*-])/;
    const minLengthRegExp   = /.{8,}/;

   if (!form.password || form.password ==="") newErrors.password = "Password is required"
    else if (!uppercaseRegExp.test(form.password))  newErrors.password = "Please provide at least one Uppercase";
    else if (!lowercaseRegExp.test(form.password))  newErrors.password = "Please provide at least one Lowercase";
    else if (!digitsRegExp.test(form.password))  newErrors.password = "Please provide at least one digit";
    else if (!specialCharRegExp.test(form.password))  newErrors.password = "Please provide at least one special character";
    else if (!minLengthRegExp.test(form.password))  newErrors.password = "Please provide at least minumum 8 characters";


       //confirm password
       if (!form.confirmPassword || form.confirmPassword ==="")  newErrors.confirmPassword ="Please confirm password"
       else if (form.confirmPassword!==form.password) newErrors.confirmPassword = "Confirm password is not matched";

    return newErrors;
    
  }
