export default function validateFormCheckout  (form) {
      
     
    let newErrors = {}

    //email
     if (!form.email || form.email ==="") newErrors.email = "Email address is required"
    else if (!/\S+@\S+\.\S+/.test(form.email))  newErrors.email = "Please provide a valid email address";

    //phone
    if (!form.phone || form.phone ==="") newErrors.phone = "Phone number is required"
    else if (! /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/.test(form.phone))  newErrors.phone = "Please provide a valid phone number";

   //first name
   if (!form.firstName || form.firstName ==="") newErrors.firstName = "Please write your first name";

   //last name
   if (!form.lastName || form.lastName ==="") newErrors.lastName = "Please write your last name";
  

   //card number
   if (!form.cardNumber || form.cardNumber ==="") newErrors.cardNumber = "Please enter your card number"
   else if (!/^[0-9]{15}(?:[0-9]{1})?$/.test(form.cardNumber))  newErrors.cardNumber = "Please enter a valid card number";

   //cvc number
   if (!form.cvc || form.cvc ==="") newErrors.cvc = "Please enter your Card Verification Value/Code"
   else if (!/^[0-9]{4}$|^[0-9]{3}$/.test(form.cvc))  newErrors.cvc= "Please enter a valid Card Verification Value/Code";

   //delivery date
  
   if (!form.pickupDate || form.pickupDate ==="") newErrors.pickupDate = "Please pick the  pick up date" ;

   //delivery time
   if (!form.pickupTime || form.pickupTime ==="---") newErrors.pickupTime = "Please pick the  pick up time" ;


   //expiry month
   if (!form.expiryMonth || form.expiryMonth ==="---") newErrors.expiryMonth = "Please pick your card expiration month";

   //expiry year
   if (!form.expiryYear || form.expiryYear ==="---") newErrors.expiryYear = "Please pick your card expiration year";

    return newErrors;
    
  }


