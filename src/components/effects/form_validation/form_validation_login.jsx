

export default function validateFormLogin (form)  {
      
     
    let newErrors = {}

    //email
     if (!form.email || form.email ==="") newErrors.email = "Email address is required"
    else if (!/\S+@\S+\.\S+/.test(form.email))  newErrors.email = "Please provide a valid email address";


    //password
    const uppercaseRegExp   = /(?=.*?[A-Z])/;
    const lowercaseRegExp   = /(?=.*?[a-z])/;
    const digitsRegExp      = /(?=.*?[0-9])/;
    const specialCharRegExp = /(?=.*?[#?!@$%^&*-])/;
    const minLengthRegExp   = /.{8,}/;

   if (!form.password || form.password ==="") newErrors.password = "Password is required"
    else if (!uppercaseRegExp.test(form.password))  newErrors.password = "Please provide at least one Uppercase";
    else if (!lowercaseRegExp.test(form.password))  newErrors.password = "Please provide at least one Lowercase";
    else if (!digitsRegExp.test(form.password))  newErrors.password = "Please provide at least one digit";
    else if (!specialCharRegExp.test(form.password))  newErrors.password = "Please provide at least one special character";
    else if (!minLengthRegExp.test(form.password))  newErrors.password = "Please provide at least minumum 8 characters";

  
    return newErrors;
    
  }



 