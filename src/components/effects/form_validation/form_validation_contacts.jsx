export default function validateFormContacts (form) {
      
     
    let newErrors = {}

    //full name
    if (!form.fullName || form.fullName ==="") newErrors.fullName = "Please write your full name";

    //message
    if (!form.message || form.message ==="") newErrors.message = "Please write a message";

    //email
     if (!form.email || form.email ==="") newErrors.email = "Email address is required"
    else if (!/\S+@\S+\.\S+/.test(form.email))  newErrors.email = "Please provide a valid email address";

    return newErrors;
  }

