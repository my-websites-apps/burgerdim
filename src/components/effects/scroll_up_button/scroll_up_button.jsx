import React, {useEffect, useState} from "react";
import {MdOutlineVerticalAlignTop} from "react-icons/md";
import './scroll_up_button.css'

function ScrollUpButton() {
  const [isVisible, setIsVisible] = useState(false)

  useEffect(() => {
    window.addEventListener('scroll', () => {
        if (window.scrollY > 100) {
            setIsVisible(true);
        } else {
            setIsVisible(false);
        }
    });
  }, []);




  const handlerScrollUp = () => {
    console.log("Hello world")
    
    if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    }
  }
  
  return (
    <div className='btn_scroll_up' onClick={handlerScrollUp} style={{display: isVisible ? 'block':'none'}}>
        <MdOutlineVerticalAlignTop />
     
     </div>
  );
}


export default ScrollUpButton;


