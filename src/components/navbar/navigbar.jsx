
import React from "react";
import "./navigbar.css"
import { useState } from "react";
import { MdClose, MdOutlineMenu, MdOutlineAccountCircle } from "react-icons/md"
import { GiHamburger } from "react-icons/gi"
import {NavLink } from "./navigbarelem";
import CartButton from "./cart_button";
import Authentification from "../authentification/authentification";


function Navbar() {
 
  const [navbarOpen, setNavbarOpen] = useState(false);
  
  function  handleToggle () {
    setNavbarOpen(prev => !prev);
  }
  const closeMenu = () => {
    setNavbarOpen(false);
  }
  const [openLogin, setLoginOpen] = useState(false);

  const  handleClick =(e) => {
      
    setLoginOpen(true);
  
  }

 
  return (
  <nav className="navBar" >
    <div className="logo"><div className="burgerLogo"><GiHamburger className="individual_button"/></div> 
      <div>
        <h3>BurgerDim</h3>
      </div>
    </div>
    <div className = "navButtons"> 
    <MdOutlineAccountCircle className="individual_button account_button" type="button" onClick={handleClick}/>
        {<Authentification openLog={openLogin} setLog={setLoginOpen}/>}

      <CartButton/>
      <button className="hamMenu" onClick={handleToggle}>{navbarOpen ? (
          <MdClose className="individual_button close" />) : (
          <MdOutlineMenu className="individual_button" /> )} 
      </button>
    </div>  

        
    <ul className={`menuNav ${navbarOpen ? " showMenu" : ""}`}>

      <li><NavLink  to='/' activestyle="true"  onClick={() => closeMenu()}  exact="true"> Home</NavLink></li>
      <li><NavLink to="/about" activestyle="true"  onClick={() => closeMenu()} exact="true"> About us</NavLink></li>
          
      <li><NavLink to="/menu" activestyle="true"  onClick={() => closeMenu()} exact="true"> Our Menu</NavLink></li>
      <li><NavLink to='/contact' activestyle="true"  onClick={() => closeMenu() } exact="true"> Contacts</NavLink></li>
    </ul>

  </nav>
   
  )
}
 

export default  Navbar

 

   /* import React from "react";
    import { Nav, NavLink, NavMenu } from "./navigbarelem";
    const Navbar = () => {
    return (
    <>
    <Nav>
    <NavMenu>
    <NavLink to="/home" activeStyle>
    Home
    </NavLink>
    <NavLink to="/menu" activeStyle>
    Menu
    </NavLink>
    <NavLink to="/about" activeStyle>
    About us
    </NavLink>
    <NavLink to="/contact" activeStyle>
    Contact Us
    </NavLink>
    </NavMenu>
    </Nav>
    </>
    );
    };
    export default Navbar;*/
