import React from "react";
import "./navigbar.css"
import { useState, useRef} from "react";
import { MdOutlineShoppingCart,
        MdDelete} from "react-icons/md"
import {NavLink } from "./navigbarelem";
import {CartState} from "../../context/context"





export default function CartButton() {
    const {
      state:{cart}, dispatch
    } = CartState();

   let [cartOpen, setCartOpen] = useState(false);

    function  handleChange () {
    setCartOpen(cartOpen=!cartOpen)
    };

    const cartMenu = useRef(null);

    const closeOpenCartMenu = (e)=>{
      if(cartMenu.current && cartOpen && !cartMenu.current.contains(e.target)){
        setCartOpen(false)
      }
    }
    
    document.addEventListener('mousedown', closeOpenCartMenu);

    return(
        <div ref={cartMenu}>
        <MdOutlineShoppingCart onClick={handleChange}  className={`individual_button cart_button`}/>
        {cartOpen && (
            cart.length > 0 ?
              (<div className="cart_list" >
                { cart.map((pr) =>
                  <span className="product_in_cart" key={pr.id}>
                    <div className="img">
                      <img src={pr.image} width="150" height="300" alt={pr.title}/>
                    </div>
                    <div className="product_in_cart_detail">
                      <span className="title">{pr.title}</span>
                      <span className="price">{pr.price}$</span>
                    </div>
                    <MdDelete 
                      fontSize="20px"
                      style={{ cursor:"pointer"}} 
                      onClick={()=>
                      dispatch({
                        type:"remove_from_cart",
                        payload:pr,
                      })}
                    />
                  </span>
  
                )}
                <NavLink to="/cart"><button className="go_to_cart">Go to cart</button></NavLink>
                </div>):
              (<div className="cart_list" > 
              Cart is empty
              </div>
            )
          )
        }
  
        <div className="badge">{cart.length}</div>
      </div>
    )
}



