
import React from 'react';
import "./home_header.css";
import imag from "../../../img/spicy_double_burgers.jpg";
import { GiHamburger } from "react-icons/gi";
import {Link } from "react-router-dom";


function HomeHeader () {
    return (
    <>
        <div className="wrapper_small">

            
                <p>Welcome to <span>BurgerDim!</span></p>
                <div><GiHamburger style={{fontSize:"10rem" }}/></div>
                <p> <Link to='/about' activestyle="true" exact="true" className="header_text small_text">
                     Who are we?
                     </Link>
                </p>
                <p><Link to='/menu' activestyle="true" exact="true" className="header_text">
                     Taste what we do!
                    </Link>
                </p>
                
            
        </div>
        <div className="wrapper_large">
            <div className="inner"> Welcome to BurgerDim!</div>
            <img src={imag} alt="hello_image" className="hello_image" />
            
        
        </div>
    </>
    )
    
    }
    
    export default HomeHeader;
