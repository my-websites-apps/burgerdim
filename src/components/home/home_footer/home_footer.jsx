import "./home_footer.css"
import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  
  faFacebook,
  faTwitter,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import {NavLink } from "../../navbar/navigbarelem"



const socialMediaHandlesLinks = {

  instagram: 'https://www.instagram.com',
  facebook: 'https://www.facebook.com',
  twitter: 'https://twitter.com'
}



function HomeFooter () {
    return (
<footer className="footer_1">
      <div>
        <div className="links_1">
          <div className="slogan">
            <h6>BurgerDim</h6>
            <p> Impress your taste!</p>
          </div>

          <div>
            
            <ul className="footer-links">
              
              <li><NavLink to="/menu" activestyle="true" exact="true"> Order Now</NavLink></li>
              <li><NavLink to="/about" activestyle="true"  exact="true"> About us</NavLink></li>
              <li><NavLink to="/menu" activestyle="true" exact="true"> Our Menu</NavLink></li>
              <li><NavLink to='/contact' activestyle="true"  exact="true"> Contacts</NavLink></li>
            </ul>
          </div>

        
        </div>
        <hr/>
      </div>
      <div>
        <div className="links_1">
          <div>
            <p className="copyright-text">Copyright &copy; 2023 All Rights Reserved by 
            Svitlana Tertychna
            </p>
          </div>

          <div className="social-menu">
            <a  href={socialMediaHandlesLinks.facebook} >
              <FontAwesomeIcon icon={faFacebook} size="2x" />
            </a>
            <a  href={socialMediaHandlesLinks.instagram}>
              <FontAwesomeIcon icon={faInstagram} size="2x" />
            </a>
            <a href={socialMediaHandlesLinks.twitter}>
              <FontAwesomeIcon icon={faTwitter}  size="2x" />
            </a>
          </div>
        </div>
      </div>
</footer>



    )
    
    }
    
    export default HomeFooter;