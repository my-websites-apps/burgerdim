import img from "../../../img/Burger_Combo.png";
import img2 from "../../../img/Burger_Angus.png"
import "./carousel.css"
import Carousel from 'react-bootstrap/Carousel';
import { NavLink } from "../../navbar/navigbarelem";



function IndividualIntervalsCarousel() {
  const itemTemplate1=()=> (
  <div className="container">
    <div className="card_text">
      <h3>Try our new delicious Cheeseburger combo!</h3>
      <p>Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in.</p>
      <button className="to_menu large_screen" >
        <NavLink to='/menu' activestyle="true" exact="true" style={{color:"#fff"}}> Order Online</NavLink>
     </button>
    </div>
    <img src={img} alt="First slide" />
    <button className="to_menu small_screen" >
        <NavLink to='/menu' activestyle="true" exact="true" style={{color:"#fff"}}> Order Online</NavLink>
    </button>
  </div>
  );

  const itemTemplate2=()=> (
    <div className="container">
      <div className="card_text">
        <h3>Our novelty: burgers of angus beef!</h3>
        <p>Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in.</p>
        <button className="to_menu large_screen" >
          <NavLink to='/menu' activestyle="tru" exact="true" style={{color:"#fff"}}> Order Online</NavLink>
        </button>
      </div>
      <img src={img2} className="resize" alt="Second slide"  />
      <button className="to_menu small_screen" >
        <NavLink to='/menu' activestyle="true" exact="true"style={{color:"#fff"}}> Order Online</NavLink>
      </button>
    </div>
    );


  return (
    <div className="carousel_main">
    <Carousel>
      <Carousel.Item interval={1900}>
      {itemTemplate1()}
        
      </Carousel.Item>
      <Carousel.Item interval={1900}>
      
      {itemTemplate2()}
         
      </Carousel.Item>
      
    </Carousel>
    </div>
  );
}

export default IndividualIntervalsCarousel;