import React, {useState, useEffect} from 'react';
import {Button, Col, Form, InputGroup, Row, Image, ListGroup} from 'react-bootstrap';
import {CartState} from "../../context/context";
import  { MdOutlinePlace, MdClose} from "react-icons/md";
import ModalWindow from '../modal/modal';
import "./checkout_forms.css";
import img from "../../img/meat-cheeseburger-with-french-fries.jpg";
import validateFormCheckout from '../effects/form_validation/form_validation_checkout';


function CheckoutForms() {
    const {
        state:{cart},
      } = CartState();

      const [total, setTotal] = useState();

      useEffect(() =>{
        setTotal(cart.reduce((acc,curr)=> acc+ Number(curr.price)*curr.qty, 0))

      }, [cart]
      );
    

      const [modalActive, setModalActive] = useState(false);

   
    /*Form validation*/

    const [form, setForm] = useState ({
      firstName:"",
      lastName:"",
      email:"",
      phone:"",
      cardNumber:"",
      cvc:"",
      pickupDate:"",
      pickupTime:"",
      expiryMonth:"",
      expiryYear:""

    })
    const [errors, setErrors] = useState({})


  

    const handleChange = (event) => {
        setForm ({
            ...form,
            [event.target.name]: event.target.value,
            
        })

        if (errors[event.target.name])
        setErrors ({
            ...errors,
            [event.target.name]:null
        })
    
      }
  

      const firstAvailableDate = new Date().toISOString().slice(0, 10);
      
      const firstFutureUnavailableDate =()=>{
           let availableDatesNumber = 8;
           let today, dd, mm, yyyy;
           today=new Date();
           dd=today.getDate()+availableDatesNumber;
           mm=addZero(today.getMonth()+1);
           yyyy=today.getFullYear();
           return yyyy+"-"+mm+"-"+dd
       }
       function addZero(i) {
        if (i < 10) {i = "0" + i}
        return i;
      }

    
    const [validated, setValidated] = useState(false);
    
    const handleSubmit = (e) => {
      
        if(e) e.preventDefault();
        e.stopPropagation();
        const formErrors = validateFormCheckout(form);

        if ( Object.keys(formErrors).length > 0)
        {
            setErrors(formErrors);
        }  else {
            setValidated(true);
            setModalActive(true);
            console.log("form submitted");
        
        }
        

        console.log(validated)
    }



     /*FormSelect Info*/


     const currentTime = new Date().getHours();
     const currentYear = (new Date()).getFullYear();

     let myLists = [

         {
            id:1,
            name:"timeSlots",
            itemList: [],
            itemStart: 10,
            itemNumber: '9',
        },
        {
            id:2,
            name:"expiryYear",
            itemList: ["---"],
            itemStart: currentYear,
            itemNumber: '10',
        },
        {
            id:3,
            name:"expiryMonth",
            itemList: ["---"],
            itemStart: '1',
            itemNumber: '11',
        }
       
   
    ]

    const pushItem = (itemId, itemList, itemStart, itemNumber) => {
        for(let x = 0; x <= itemNumber; x++) {

            if (itemId === 1) {
            
                itemList.push(parseInt(itemStart) + x + ":00-" + parseInt(itemStart+1+ x)  + ":00"  ) 
            } else
            {itemList.push(parseInt(itemStart) + x)}

           
        }
        return itemList;
    }


     let myListTemp = myLists.map(el=> {
        el.itemList = pushItem(el.id, el.itemList, el.itemStart, el.itemNumber);
        
        return el;
        
    })


    
    
    function filterSlots(SlotCollection) {

       let arr=["---"];
       for (let x = 0; x < SlotCollection.length; x++) {
        let slot = SlotCollection[x];
        if(form.pickupDate===firstAvailableDate){
            
            let startTime = slot.substring(0, slot.indexOf(':'));
            
            let time = parseInt(startTime);
            if ( time > currentTime+1 ) {
                arr.push(slot)
                
            }}else{arr.push(slot)}
            
        }
        //console.log(arr);
        return arr;
    }

     myListTemp[0].itemList = filterSlots(myListTemp[0].itemList);
    
   
    let myListFull = myListTemp.map(el=> {
       
        let func = (x) => {return(<option key={x}>{x}</option>)}
        return el.itemList.map(func);   

    })
  
    const [ timeList, yearList, monthList] =  myListFull;
    let card = form.cardNumber;


    return (
        <>
            <div className ="header_block"> 
                <h1 className = "header_1">Checkout</h1>
            </div>
            
            <Form noValidate validated={validated} onSubmit={handleSubmit} action="./index.php" >
            <div className="checkout_container">
                <div className="form">
                    <h4>CONTACT</h4>
                    <Row className="mb-4">
                        <Form.Group as={Col} md="5" controlId="firstName">
                        
                            <Form.Label>First name</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="John"
                                name="firstName"
                                value={form.firstName}
                                isInvalid={errors.firstName}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.firstName}</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="5" controlId="lastname">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="Smith"
                                name="lastName"
                                value={form.lastName}
                                isInvalid={errors.lastName}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.lastName}</Form.Control.Feedback>
                            
                        </Form.Group>
                    </Row>
                    <Row className="mb-4">
                        <Form.Group as={Col} md="5" controlId="email">
                            <Form.Label>E-mail</Form.Label>
                            <InputGroup hasValidation>
                                <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                                <Form.Control
                                type="text"
                                placeholder="john@gmail.com"
                                aria-describedby="inputGroupPrepend"
                                required
                                name="email"
                                value={form.email}
                                isInvalid={errors.email}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback type="valid">Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.email}</Form.Control.Feedback>
                                
                            </InputGroup>
                        
                        </Form.Group>
                        <Form.Group as={Col} md="5" controlId="phone">
                            <Form.Label>Phone number</Form.Label>
                                <Form.Control
                                type="tel"
                                placeholder="0123456789"
                                required
                                name="phone"
                                value={form.phone}
                                isInvalid={errors.phone}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback type="valid">Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.phone}</Form.Control.Feedback>
                        </Form.Group>

                    </Row>
                    <h4>PICK UP</h4><a href="https://goo.gl/maps/xW5AEXCpnvxydG8NA" style={{color:"black", textDecoration:"none"}}>
                        <MdOutlinePlace style={{width: "30px", height: "30px", marginBottom:"20px"}}/>at Khreshchatyk St, 1, 3rd Floor, Kyiv</a>
                    <Row className="mb-4">
                        <Form.Group as={Col} md="5" lg="5">
                            <Form.Label>Pick up date</Form.Label>
                            <Form.Control 
                                type="date"
                                name='pickupDate' 
                                fluid ="true"
                                label='Date'
                                min={firstAvailableDate}
                                max={firstFutureUnavailableDate()}
                                value={form.pickupDate}
                                isInvalid={errors.pickupDate}
                                onChange={handleChange}
                            />
                        <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">{errors.pickupDate}</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="5" lg="5">
                            <Form.Label>Pickup time</Form.Label>
                            <Form.Select
                                type="time"
                                name='pickupTime' 
                                fluid ="true"
                                label='Time'
                                value={form.pickupTime}
                                isInvalid={errors.pickupTime}
                                onChange={handleChange}>  

                                {timeList}    
                            </Form.Select>
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.pickupTime}</Form.Control.Feedback>
                        </Form.Group>

                    </Row>
                    
                    <h4>PAYMENT</h4>

                    <Row className="mb-4">
                        <Form.Group as={Col} md="5" lg="5">
                            <Form.Label>Card Number</Form.Label>
                            <Form.Control
                                name='cardNumber'
                                type='number' 
                                placeholder='0000111100001111' 
                                value={form.cardNumber}
                                isInvalid={errors.cardNumber}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.cardNumber}</Form.Control.Feedback>
                        </Form.Group>
                    </Row>
                    <Row className="mb-2">
                    
                    
                        <Form.Group as={Col} md="3" lg="3">
                        <Form.Label>Expiry Month</Form.Label>
                            <Form.Select 
                                name='expiryMonth' 
                                fluid ="true"
                                label='Month'
                                value={form.expiryMonth}
                                isInvalid={errors.expiryMonth}
                                onChange={handleChange}
                                >
                                {monthList}
                            </Form.Select> 
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.expiryMonth}</Form.Control.Feedback>
                        </Form.Group>
                        
                        <Form.Group as={Col} md="4" lg="4">
                        <Form.Label>Expiry Year</Form.Label>
                            <Form.Select 
                                name='expiryYear' 
                                fluid ="true"
                                label='Year' 
                                value={form.expiryYear}
                                isInvalid={errors.expiryYear}
                                onChange={handleChange}
                                >
                                    {yearList} 
                            </Form.Select> 
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.expiryYear}</Form.Control.Feedback>
                        </Form.Group>  
                        
                        <Form.Group as={Col} md="3" lg="3">
                        <Form.Label>CVC/CVV </Form.Label>
                            <Form.Control
                                width={3} 
                                name='cvc'
                                type='text'
                                label='CVC/CVV' 
                                placeholder='123'
                                value={form.cvc}
                                isInvalid={errors.cvc}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">{errors.cvc}</Form.Control.Feedback>
                            
                        </Form.Group>
                    </Row>
                   
                </div>
                <div className = "order_summary">
                    <h4>ORDER SUMMARY</h4>
                    <ListGroup style={{margin:"0"}}>
                        { cart.map((pr) =>
                        <ListGroup.Item key={pr.id}>
                            <Row className="mb-3">
                                <Col md ={2} lg={2} sm={2} xs={2}><span><Image src={pr.image} alt={pr.name} fluid rounded width={100} height={100} ></Image></span>
                                </Col>
                                <Col md ={6} lg={4}sm={4} xs={4}><span >{pr.title}</span>
                                </Col>
                                    
                                <Col md ={2} lg = {3} sm={2} xs={2}><span> ${pr.price}</span></Col>
                                <Col md ={2} lg = {3} sm={2} xs={2}><span> {pr.qty}</span></Col>

                            </Row>
                            
                            
                        </ListGroup.Item>
                        )}
                        <ListGroup.Item><Row className="mb-3">Subtotal ({cart.length}) items </Row>
                        <Row className="mb-3">Total: ${total} </Row>
                        </ListGroup.Item>
                    </ListGroup>
                    
                    <Button type="submit" disabled={cart.length === 0 || validated===true} className="to_thank_you" >
                        Place Order
                    </Button>
                        
                    
                </div>
                
            </div>   
            </Form>
            <ModalWindow active={modalActive}  setActive={setModalActive} >
                
                <div className="modal_wind_content_receipt">
                
                    <div className="thank_you_text">

                        <h6>Thank you for ordering, {form.firstName}!</h6>
                        <p>Here's your receipt</p>

                        <div> Payment info</div>
                        <p>Card ending in  {card.substring(12, 16)}</p>

                        <div>Pick up info</div>
                        <p>Pick up from <span><a href="https://goo.gl/maps/xW5AEXCpnvxydG8NA" style={{color:"black", textDecoration:"none"}}>
                                <MdOutlinePlace style={{width: "20px", height: "20px", marginBottom:"5px"}}/> Khreshchatyk St, 1, 3rd Floor, Kyiv</a></span>
                                <span >&nbsp;</span>at this time range: {form.pickupTime}</p>
                                
                    </div>    
                    <div className="ordered_products" >
                        <img src={img} alt="thank_you_image" className="thank_you_image"  />
                        
                        <ListGroup style={{margin:"0", borderRadius:"0px"}}>
                            { cart.map((pr) =>
                            <ListGroup.Item key={pr.id} style={{backgroundColor:"var(--color-beige)"}}>
                                <Row className="mb-1">
                                    <Col md ={2} lg={3} sm={2} xs={3}>
                                        <span>
                                            <Image src={pr.image} alt={pr.name} fluid rounded width={100} height={100} ></Image>
                                        </span>
                                    </Col>
                                    <Col md ={4} lg={5} sm={4} xs={5}><span >{pr.title}</span>
                                    </Col>
                                            
                                    <Col md ={2} lg = {2} sm={2} xs={2}><span> ${pr.price}</span></Col>
                                     <Col md = "auto" lg = "auto" sm={2} xs={2}><span> {pr.qty}</span></Col>

                                </Row>
                                    
                                    
                            </ListGroup.Item>)}
                            <ListGroup.Item style={{backgroundColor:"var(--color-brown)", color:"white", fontSize:"1.2rem"}}>
                                <Row className="mb-3">Total: ${total} </Row>
                            </ListGroup.Item>
                        </ListGroup>

                    </div>
                    <MdClose className="close" onClick={()=> setModalActive(false)}></MdClose>
                </div>
                
            
            </ModalWindow>
        </>
      );
    
}

export default CheckoutForms;


