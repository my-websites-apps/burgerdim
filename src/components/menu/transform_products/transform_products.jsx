
import { CartState } from "../../../context/context";

const transformProducts = (inform) => {
    const {state: {products},
    productState: {sorting, byStock, byVegeterian, byRating, searchQuery} } = CartState();

    
    
    let sortedProducts = products.get(inform);
    
    if (sorting) {
        sortedProducts = sortedProducts.sort ((a,b) => sorting === "lowToHigh" ? a.price - b.price : b.price - a.price);
    }

    if (byStock) {
        sortedProducts = sortedProducts.filter((prod) => prod.inStock !=="0");

    }

    if (byVegeterian) {
        sortedProducts = sortedProducts.filter((prod) => prod.vegeterian);

    }

    if (byRating) {
        sortedProducts = sortedProducts.filter (
            (pr)=> pr.rate === byRating);

    }

    if (searchQuery) {
        sortedProducts = sortedProducts.filter (
            (pr)=> pr.title.toLowerCase().includes(searchQuery) );

    }

    return sortedProducts;
}
export default transformProducts;