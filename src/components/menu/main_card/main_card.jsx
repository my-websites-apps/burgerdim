import React from 'react';
import "./main_card.css"


const MainCard = (props) => {
    /*console.log(props)*/
    return (
        
    <div className="main_card">
        <a href={props.inform.link}>
            <div className="main_card-img">
                <img src={props.inform.imag} width="150" height="300" alt="img"/>
            </div>
            <h2 className="main_card-title">{props.inform.title}</h2>
        </a> 
    </div>

)

}

export default MainCard