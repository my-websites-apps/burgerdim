import React from 'react';
import ProductCard from "../product_card/product_card";
import "./product_group.css";
import "../../effects/header/header.css";




function FryMenu (props) {
    const {meals3} = props;
    
return (
    <>
        <div className ="header_block" id="fries"> 
                    <h1 className = "header_1">Fries</h1>
        </div>
        <div className="container_single">
        
            {
                meals3.map ((el) => {
                    return <ProductCard key={el.id} meal1={el}></ProductCard>
                })

            }
        
        </div>
    </>
)

}

export default FryMenu;