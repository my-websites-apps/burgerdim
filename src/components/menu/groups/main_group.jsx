import React from 'react';
import MainCard from "../main_card/main_card";
import mainInfo from "../../../code/main_menu_data";
import "./main_group.css";
import "../../effects/header/header.css";
import Search from "../search/search";
import Filters from '../filters/filters';


function MainGroup () {
   

return (
    <>
    <div className ="header_block"> 
        <h1 className = "header_1">Our Menu</h1>
    </div>
    <Filters/>
    <Search/>
  
    
    <div className="container_main">
        <div className="container_main_card">
            {
                mainInfo.map ((el,ind) => {
                    return <MainCard key={el.id} inform={el}></MainCard>
                })

            }
        </div>
    </div>
    </>
)

}

export default MainGroup;