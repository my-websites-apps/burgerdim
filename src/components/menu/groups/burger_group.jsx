import React from 'react';
import ProductCard from "../product_card/product_card";
import "./product_group.css";
import "../../effects/header/header.css";

function BurgerMenu (props) {
    const {meals1} = props;
  
return (
    <>
        <div className ="header_block" id="burgers"> 
            <h1 className = "header_1" >Burgers</h1>
        </div>
        
        <div className="container_single">
   
            {
             meals1.map ((el) => {
                return <ProductCard key={el.id} meal1={el}  ></ProductCard>
                })

            }
        </div>
    </>
    
)

}

export default BurgerMenu;