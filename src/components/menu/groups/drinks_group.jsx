import React from 'react';
import ProductCard from "../product_card/product_card";
import "./product_group.css";
import "../../effects/header/header.css";


function DrinksMenu (props) {
    const {meals2} = props;
return (
    <>
        <div className ="header_block" id="drinks"> 
            <h1 className = "header_1">Drinks</h1>
        </div>
        <div className="container_single">
    
            {
                meals2.map ((el) => {
                 return <ProductCard key={el.id} meal1={el} ></ProductCard>
                })

            }
    
        </div>
    </>
)

}

export default DrinksMenu;