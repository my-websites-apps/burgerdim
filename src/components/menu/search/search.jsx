
import React, {useState} from 'react';
import {MdSearch, MdClose} from "react-icons/md";
import "./search.css";
import Tooltip from '../../effects/tooltip/tooltip';
import {CartState} from "../../../context/context";



const Search = () => {
    const {
        productState:{searchQuery}, productDispatch
      } = CartState();
      console.log (searchQuery);


      let [searchOpen, setSearchOpen] = useState(false);

      function  handleChange () {
              setSearchOpen(searchOpen=!searchOpen)
        };

    return (
    <div className="search_container">
        <div className="search_box">
        
            <span className="search_button" onClick={handleChange} >
                
            {!searchOpen ? (<Tooltip content="Search meals" direction="left"><MdSearch></MdSearch></Tooltip>): 
            (<MdClose style={{ color: "#fff", width: "30px", height: "30px" }} />)} 
            </span>
                
            {searchOpen ? ( 
            <input type="search" 
                id="search_field_active"
                
                placeholder="Search..."
                onChange={(e) =>{
                    productDispatch ({

                        type:"sort_by_search",
                        payload: e.target.value,
                    })
                }}
                
            />): (<input type="search" 
            id="search_field_unactive"
            />)}
        </div>
    </div>)
}

export default Search

