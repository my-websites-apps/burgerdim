import React from "react"
import {MdStarOutline, MdStarRate  } from "react-icons/md";



 const StarRating = ({rating, onClick, style}) =>{
 
  
  return (
    <div>
      {[1, 2, 3, 4, 5].map((value) => (
        <span key={value} onClick={() => onClick(value)} style={style}>
       { value <= rating ?(<MdStarRate/>):(<MdStarOutline/>)} </span>

      ))}

    </div>
  )

}

export default StarRating


/*
import { useState } from "react";
import Star from "./star";

function StarRating({ onChange}) {
  const [rating, setRating] = useState(0); 
  
   const changeRating = (newRating) => {
    setRating(newRating);
    onChange?.(newRating);
    
   
  };  


  return (
    <span>
      {[1, 2, 3, 4, 5].map((value) => (
        <Star
          key={value}
          filled={value <= rating}
          onClick={() => changeRating(value)}
         
        />
      ))}
    </span>
  );    
 
}
export default StarRating;
*/
/*
return (
  <span>
    {[1, 2, 3, 4, 5].map((value) => (
      <Star
        key={value}
        filled={value <= rating}
        onClick={() => changeRating(value)}
       
      />
    ))}
  </span>
);    

*/



/*

import {MdStarOutline, MdStarRate  } from "react-icons/md";

function Star({ filled, onClick }) {
    return (
      filled ? (
        <MdStarRate  onClick={onClick} />
      ) : (
        <MdStarOutline onClick={onClick} />
      )
    
    );
  }
  export default Star;

*/





/*

  useEffect(() => {
    setRating(JSON.parse(window.localStorage.getItem('rating')));
  }, []);
  
  useEffect(() => {
    window.localStorage.setItem('rating', rating);
  }, [rating]);*/


