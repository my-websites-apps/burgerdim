import React, { useState, useRef } from 'react';
import {Button, Form} from 'react-bootstrap'
import "./filters.css"
import { MdFilterListAlt } from "react-icons/md";
import { CartState} from '../../../context/context';
import ClickOutside from '../../effects/click_outside/click_outside';
import Tooltip from "../../effects/tooltip/tooltip";
import StarRating from "../star_rating/star_rating";

const Filters = () => {
  
  const {productState:{byStock, byRating, sorting, byVegeterian}, productDispatch} = CartState();
  
  console.log (byStock, byRating, sorting, byVegeterian);
  
  
  const wrapperRef = useRef(null);
  ClickOutside(wrapperRef, () => {
    setFilterOpen(false);
  });

   
    let [filterOpen, setFilterOpen] = useState(false);

    function  handleChange () {
            setFilterOpen(filterOpen=!filterOpen)
          };
        
          
   
    
  return (
        <div ref={wrapperRef}>
        
          <div className="filter_button_container">
            <Tooltip content="Filter meals" direction="right">
              <MdFilterListAlt onClick={handleChange}   className={` filter_button ${filterOpen && `active`}`}/> 
            </Tooltip></div>
         {filterOpen && (
          <div className="filters activ">
            <span>
                <Form.Check
                inline
                label="Ascending"
                name="group1"
                type="radio"
                id={`inline-2`}
                onChange={()=>
                  productDispatch({
                    type:"sort_by_price",
                    payload:"lowToHigh",
                  })
                }
                checked={sorting === "lowToHigh" ? true: false}
                />
            </span>

            <span>
                <Form.Check
                inline
                label="Descending"
                name="group1"
                type="radio"
                id={`inline-2`}
                onChange={()=>
                  productDispatch({
                    type:"sort_by_price",
                    payload:"highToLow",
                  })
                }
                checked={sorting === "highToLow" ? true: false}
                />
            </span>
            <span>
                <Form.Check
                inline
                label="Show available"
                name="group1"
                type="checkbox"
                id={`inline-3`}
                onChange={()=>
                  productDispatch({
                    type:"sort_by_stock",
                    
                  })
                }
                checked={byStock}
                />
            </span>
            
            <span>
                <Form.Check
                inline
                label="Vegeterian"
                name="group1"
                type="checkbox"
                id={`inline-3`}
                onChange={()=>
                  productDispatch({
                    type:"sort_by_vegeterian",
                    
                  })
                }
                checked={byVegeterian}
                />
            </span>

            <span>
                <label style = {{paddingRight:10}}>Rating:</label>

                {<StarRating 
                
                rating={byRating}
                onClick= {(value) => 
                  productDispatch({
                    type:"sort_by_rating",
                    payload:(value),
                  })}
                  style={{cursor: "pointer"}}
                  
                  />}
              </span>
            <Button variant="light"
            onClick={()=>
              productDispatch({
                type:"clear_filters",
              })
            }
            >Clear Filters</Button>
          </div>)}
        </div>
      )
}

export default Filters

