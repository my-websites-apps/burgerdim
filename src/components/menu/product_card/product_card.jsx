import React, {useState} from 'react';
import "./product_card.css"
import StarRating from '../star_rating/star_rating';
import { CartState } from "../../../context/context"

const ProductCard = (props) => {
    const {meal1} = props;
    const {state:{cart}, dispatch,} = CartState()

    const [rater, setRate] = useState(meal1.rate);
    
  
    const changeRating = (newRating) => {
     setRate(newRating);}


    return (
    <div className="card_container">
        <div className="product_card">
            <div className="img">
                <img src={meal1.image} alt={meal1.title}/>
            </div>
            <h2 className="title">{meal1.title}</h2>
            <StarRating  rating={rater} onClick={(val)=>changeRating(val)}/>
            <h3 className="price">{meal1.price}$</h3>
        </div>
        <div>
            {
                cart.some((i )=> i.id === meal1.id) ?
                (<button onClick = {() => {
                    dispatch({
                        type: "remove_from_cart",
                        payload: meal1,
                    })
                    }} className="card_button remove_from_cart">Remove from Cart
                </button>) : (
                    <button onClick = {() => {
                        dispatch({
                            type: "add_to_cart",
                            payload: meal1,
                        })
                        }} 
                        disabled={meal1.inStock==="0"} className="add_in_cart card_button ">
                        {meal1.inStock==="0" ? "Currently unavailable": "Add to Cart" }
                    </button>  
                    )
                    
            }
             
        </div>
    </div>

)

}

export default ProductCard