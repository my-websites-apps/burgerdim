import React from "react";
import "./modal.css";

const ModalWindow =({active, setActive, children})=>{

    return (
        <div className={active ? "modal_wind act" : "modal_wind"} onClick={()=> setActive(true)}>
            <div className={active ? "modal_wind_content act" : "modal_wind_content"} onClick={e=>e.stopPropagation()}>
            {children}
            </div>
        </div>
    );

};
export default ModalWindow

