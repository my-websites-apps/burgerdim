import React, {useState} from 'react';
import {Button,  Form } from 'react-bootstrap';
import "./account.css";
import validateFormRegistration from '../effects/form_validation/form_validation_registration';


export default function Registration() {
   

/*Form validation*/

  const [form, setForm] = useState ({
    email:"",
    fullName:"",
    phone:"",
    password:"",
    confirmPassword:"",

  })

  const [errors, setErrors] = useState({})


  const handleChange = (event) => {
      setForm ({
          ...form,
          [event.target.name]: event.target.value,
          
      })

      if (errors[event.target.name])
      setErrors ({
          ...errors,
          [event.target.name]:null
      })
  
    }

  
  const [validated, setValidated] = useState(false);




  const handleSubmit = (e) => {
    
      if(e) e.preventDefault();
      e.stopPropagation();
      const formErrors = validateFormRegistration(form);
      //console.log(Object.keys(formErrors));


      if ( Object.keys(formErrors).length > 0)
      {
          setErrors(formErrors);
      }  else {
        setValidated(true);
      console.log("form submitted");
      
      }
      

      console.log(validated)
    }


    return (
      
                   
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicName" action="./index.php">
                <Form.Label >
                    Full name
                </Form.Label>
                <Form.Control 
                type="text"
                placeholder="Enter your full name"
                name="fullName"
                value={form.fullName}
                isInvalid={errors.fullName}
                onChange={handleChange} />
                <Form.Control.Feedback  type="valid"> Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">{errors.fullName}</Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label >
                    Email address
                </Form.Label>
                <Form.Control 
                type="e-mail" 
                placeholder="Enter your e-mail"
                name="email"
                value={form.email}
                isInvalid={errors.email}
                onChange={handleChange} />
                    <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">{errors.email}</Form.Control.Feedback>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPhone">
                <Form.Label >
                    Phone number
                </Form.Label>
                <Form.Control 
                type="tel" 
                placeholder="Enter your phone number"
                name="phone"
                value={form.phone}
                isInvalid={errors.phone}
                onChange={handleChange}/>
                <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">{errors.phone}</Form.Control.Feedback>
            </Form.Group>
            
            <Form.Group className="mb-3"controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                type="password"
                placeholder="Enter password"
                name="password"
                value={form.password}
                isInvalid={errors.password}
                onChange={handleChange} />
                <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">{errors.password}</Form.Control.Feedback>
            </Form.Group>
            
            <Form.Group className="mb-3"controlId="formBasicPassword">
                <Form.Label>Confirm password</Form.Label>
                <Form.Control 
                type="password"
                placeholder="Enter password again"
                name="confirmPassword"
                value={form.confirmPassword}
                isInvalid={errors.confirmPassword}
                onChange={handleChange}  />
                <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">{errors.confirmPassword}</Form.Control.Feedback>
            </Form.Group>
            <div>
            <Button variant="primary" type="submit" className="login_button">
                Sign Up
            </Button>
            </div>
        </Form>
        
                        
        
    );
}
  