
import React, {useState} from 'react';
import {Col,  Row} from 'react-bootstrap';
import  { MdClose} from "react-icons/md";
import ModalWindow from '../modal/modal';
import Registration from './registration';
import Login from './login';
import "./account.css";

export default function Authentification({openLog, setLog}) {

  
    let [authMode, setAuthMode] = useState("signin")
  
    const changeAuthMode = () => {
      setAuthMode(authMode === "signin" ? "signup" : "signin")
    }

    if (authMode === "signup"){

        return (
          
            <ModalWindow active={openLog}  setActive={setLog} >
                <div className="modal_wind_content_login">
                    <Row className=" d-flex justify-content-center align-items-center">
                        <Col xs={10} md={10} lg={12}  xl={16}>

                            <h2 >BurgerDim</h2>
                            <p style={{padding:" 0 1em"}}>Please fill the form!</p>
                            <Registration/>
                             <div>Already have an account?{" "}
                                <span  onClick={changeAuthMode} className="links">
                                    Sign In
                                </span>
                            </div>
                            
                        </Col>
                    </Row>
                    <MdClose className="close" onClick={()=> setLog(false)}></MdClose>
                </div>
                
            </ModalWindow>


          );}
      
        return (
      
        <ModalWindow active={openLog}  setActive={setLog} >
            <div className="modal_wind_content_login">
               <Row className=" d-flex justify-content-center align-items-center">
                    <Col xs="auto"  md={10} lg={12}  xl={12}>
                        
                        <h2 >BurgerDim</h2>
                        
                        <Login className="reset"/>
                        <div>Not registered yet?{" "}
                            <span onClick={changeAuthMode} className="links">
                                Sign Up
                            </span>
                            
                        </div>
                    </Col>
                </Row>
                <MdClose className="close" onClick={()=> setLog(false)}></MdClose>
            </div>
        
      </ModalWindow>
  )
}