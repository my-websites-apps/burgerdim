import React, {useState} from 'react';
import {Button, Form } from 'react-bootstrap';
import "./account.css";
import validateFormLogin from '../effects/form_validation/form_validation_login';



export default function Login() {


  let [passwordMode1, setPasswordMode1] = useState("remember")

  const changePasswordMode1 = () => {
    setPasswordMode1(passwordMode1 === "remember" ? "forgot" : "remember")
  }

  
/*Form validation*/

  const [form1, setForm1] = useState ({
    email:"",
    password:"",

  })


  const [errors1, setErrors1] = useState({})



    const handleChange1 = (event) => {
        setForm1 ({
            ...form1,
            [event.target.name]: event.target.value,
            
        })
  
        if (errors1[event.target.name])
        setErrors1 ({
            ...errors1,
            [event.target.name]:null
        })
  
    
      }
  


   const [validated1, setValidated1] = useState(false);

   const handleSubmit1 = (e) => {
    
    if(e) e.preventDefault();
    e.stopPropagation();
    const formErrors1 = validateFormLogin(form1);
    //console.log(Object.keys(formErrors1));


    if ( Object.keys(formErrors1).length > 0)
    {
        setErrors1(formErrors1);
        console.log(formErrors1)
    }  else {
      setValidated1(true);

    console.log("form submitted");
    
    }
    
    console.log(validated1)
  }

  if (passwordMode1 === "forgot"){

    return (
      <div >
      <p>Please enter your email. You will receive a link to create a new password via email.</p>
       <Form noValidate validated={validated1} onSubmit={handleSubmit1} action="./index.php">
       
      <Form.Group className="mb-3" controlId="formBasicEmail1"  >
          <Form.Label >
            Email address
          </Form.Label>
          <Form.Control 
          type="e-mail" 
          placeholder="Enter email"
          name="email"
          value={form1.email}
          isInvalid={errors1.email}
          onChange={handleChange1} />
          <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
          <Form.Control.Feedback type="invalid">{errors1.email}</Form.Control.Feedback>
      </Form.Group><div >
            <Button variant="primary" type="submit" className="login_button">
                Reset password
            </Button>
        </div>
    </Form>
    </div>
    );


   
    

  }

  return (
    <>
    <p>Please enter your email and password!</p>
    <Form noValidate validated={validated1} onSubmit={handleSubmit1}>
        <Form.Group className="mb-3" controlId="formBasicEmail1">
            <Form.Label >
              Email address
            </Form.Label>
            <Form.Control 
            type="e-mail" 
            placeholder="Enter email"
            name="email"
            value={form1.email}
            isInvalid={errors1.email}
            onChange={handleChange1} />
            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">{errors1.email}</Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3"controlId="formBasicPassword1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
            type="password"
            placeholder="Enter password"
            name="password"
            value={form1.password}
            isInvalid={errors1.password}
            onChange={handleChange1} />
            <Form.Control.Feedback > Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">{errors1.password}</Form.Control.Feedback>
        </Form.Group>
        
        <Form.Group className="mb-3"controlId="formBasicCheckbox">
                <span className="links" onClick={changePasswordMode1}>
                Forgot password?
                </span>
        </Form.Group>
        <div >
            <Button variant="primary" type="submit" className="login_button">
                Sign In
            </Button>
        </div>
    </Form>
    </>             
         
  )
}
