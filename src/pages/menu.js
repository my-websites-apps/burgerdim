import React from "react";
import MainMenu from "../components/menu/groups/main_group";
import BurgerMenu from "../components/menu/groups/burger_group";
import FryMenu from "../components/menu/groups/fries_group";
import DrinksMenu from "../components/menu/groups/drinks_group";
import "../components/effects/main/main.css";
import ScrollUpButton from "../components/effects/scroll_up_button/scroll_up_button";
import transformProducts from "../components/menu/transform_products/transform_products";



  
   const Menu = () => {
    return (
    <main className="main_white">
        <div>
            <MainMenu/> 
        </div>
            
        <div>        
            <BurgerMenu meals1={transformProducts('burgerInfo')}/>
            
        </div>
        <div>
            <FryMenu meals3={transformProducts('fryInfo')}/>
        </div>
        <div>
            <DrinksMenu meals2={transformProducts('drinkInfo')}/>
         </div>
         <ScrollUpButton/>
    </main> 

    
    );
    
};

export default Menu;