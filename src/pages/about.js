
    import React from "react";
    import AboutUs from "../components/about/about_us"
    import "../components/effects/header/header.css";
    import "../components/effects/main/main.css";
    

    const About = () => {
      
        return (
        <main className='main_white'>
             <AboutUs/>
        </main>
        );
    };
    
    export default About;
