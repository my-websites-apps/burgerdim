import React from 'react';
import "../components/effects/main/main.css";
import CheckoutForms from '../components/checkout/checkout_forms';


const Checkout = () => {

  
    return (
    <>
        <main className='main_white'>
            <CheckoutForms/>
        </main>
        
     </>
    );
};

export default Checkout;
