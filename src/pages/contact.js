import React from "react";
import "../components/effects/main/main.css";
import Contacts from "../components/contacts/contacts";


const Contact = () => {
    return (
    <main className='main_white'>
        <Contacts/>
    </main>
    );
};

export default Contact;
