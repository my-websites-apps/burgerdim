 
    import React from 'react';
    import "../components/effects/main/main.css";
    import IndividualIntervalsCarousel from "../components/home/carousel/carousel";
    import HomeHeader from "../components/home/home_header/home_header"
    import HomeFooter from "../components/home/home_footer/home_footer"
    
    const Home = () => {
        
    
        return (
        <main className='main_orange'>
            <HomeHeader />
            <IndividualIntervalsCarousel />
            <HomeFooter/>
            
        </main>
        );
    };

    export default Home;
