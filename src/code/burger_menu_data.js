import img1 from "../img/Burger1.png"
import img2 from "../img/Burger2.png"
import img3 from "../img/Burger3.png"
import img4 from "../img/Fry1.png"
import img5 from "../img/Fry2.png"
import img6 from "../img/Drink1.png"
import img7 from "../img/Drink2.png"
import img8 from "../img/Drink3.png"





const data  = new Map ([
    ['burgerInfo', [
        {   id:"1",
            image: img1,
            title:"BurgerDim Hamburger",
            price:"2.5",
            rate: 4,
            inStock:"15",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"2",
            image: img2,
            title:" BurgerDim Cheeseburger",
            price:"3",
            rate: 5,
            inStock:"17",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"3",
            image: img3,
            title:" BurgerDim Vegiburger",
            price:"1.5",
            rate: 2,
            inStock:"10",
            vegeterian:"true",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        }
    ]],
    
    ['fryInfo', [ 
        {   id:"4",
            image: img4,
            title:"BurgerDim Fries#1",
            price:"1.25",
            rate:3,
            inStock:"14",
            vegeterian:"true",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"5",
            image: img5,
            title:" BurgerDim Fries#2",
            price:"1.5",
            rate: 4,
            inStock:"32",
            vegeterian:"true",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        }
    ]],
        
    
    
    ['drinkInfo', [
        {   id:"6",
            image: img6,
            title:"Iced Cola",
            price:"1.3",
            rate: 3,
            inStock:"20",
            vegeterian:"true",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"7",
            image: img7,
            title:"Iced Cola & Lemon",
            price:"1.8",
            rate: 5,
            inStock:"0",
            vegeterian:"true",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"8",
            image: img8,
            title:"Iced Tea & Lemon",
            price:"1.5",
            inStock:"7",
            rate: 4,
            vegeterian:"true",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        }
    
     
    ]]])



/*
const data = {



burgerInfo: [
        {   id:"1",
            image: img1,
            title:"BurgerDom Hamburger",
            price:"2.5$",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"2",
            image: img2,
            title:" BurgerDom Cheeseburger",
            price:"3$",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        },
        {   id:"3",
            image: img3,
            title:" BurgerDom Vegiburger",
            price:"1.5$",
            desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
        }
    
     
    ],

fryInfo: [ 
    {   id:"4",
        image: img4,
        title:"BurgerDom Fries#1",
        price:"1.25$",
        desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
    },
    {   id:"5",
        image: img5,
        title:" BurgerDom Fries#2",
        price:"1.5$",
        desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
    }
],
    


 drinkInfo: [
    {   id:"6",
        image: img6,
        title:"Iced Cola",
        price:"1.3$",
        desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
    },
    {   id:"7",
        image: img7,
        title:"Iced Cola & Lemon",
        price:"1.5$",
        desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
    },
    {   id:"8",
        image: img8,
        title:"Iced Tea & Lemon",
        price:"1.8$",
        desc:"Lorem ipsum dolor sit amet, eam modo scripserit at, recusabo facilisis expetendis vis ei. Cum cu dicunt forensibus, vix no option propriae. Autem consul vivendum ne nam. Quo no populo impedit deserunt.Vix id propriae quaestio. Mundi fuisset voluptua id mea. Salutandi voluptaria usu ex, qui legimus omnesque in. Eam vidit affert possim in."
    }

 
]
}



*/





 export default data;