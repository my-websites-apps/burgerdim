

export const cartReducer = (state, action)=>{
    let newState;
switch (action.type) {
    case "add_to_cart":
         newState = {...state, cart:[...state.cart, {...action.payload, qty:1}]};
         break;
    case "remove_from_cart":
         newState = {
            ...state,
            cart: state.cart.filter ((x) =>
             x.id!== action.payload.id),
        }; break;
        case "change_pr_qty_in_cart":
         newState = {
            ...state,
            cart: state.cart.filter ((x) => x.id=== action.payload.id?(x.qty= action.payload.qty):x.qty),
        }; break;
    default:
 return state;
}
   return newState;

}

export const productReducer = (state, action) => {
    switch(action.type) {

        case "sort_by_price":
            return {...state, sorting:action.payload};
        case "sort_by_stock":
            return {...state, byStock:!state.byStock};
        case "sort_by_vegeterian":
            return {...state, byVegeterian:!state.byVegeterian};
        case "sort_by_rating":
            return {...state, byRating:action.payload};
        case "sort_by_search":
            return {...state, searchQuery: action.payload};
        case "clear_filters":
            return {
                byStock:false,
                byRating:0,
                byVegeterian:false,
                searchQuery:"",
            };  
        default:
            return state;

    }



};
