import {createContext, useReducer, useContext, useEffect} from "react"
import {cartReducer} from "./reducers"
import data from "../code/burger_menu_data"
import { productReducer} from "./reducers"



const Cart = createContext()

const Context = ({children}) => {
    const products  = data;
  

   
    const getLocalCartData = () => {
        let localCartData = localStorage.getItem('CartItems');
        //console.log(localCartData)
        if (localCartData === [] || localCartData === null) {
          return [];
        } else {
          return JSON.parse(localCartData);
        }
      }


    const [state, dispatch] = useReducer(cartReducer, {

            products:products,
            cart:getLocalCartData(),
        }
       
        )
        //console.log(state.cart);


    const [productState, productDispatch] = useReducer(productReducer, {
        byStock: false,
        byVegeterian: false,
        byRating: 0,
        searchQuery:"",
    });

    useEffect(() => {
        localStorage.setItem('CartItems', JSON.stringify(state.cart));
      }, [state.cart]);


    
       

    return (
       
        <Cart.Provider value ={{state, dispatch, productState, productDispatch}}> {children}</Cart.Provider>

    )

}


export default Context;


export const CartState = () => {
    return useContext(Cart);

}