
    import React from 'react';
    import Navbar from '././components/navbar/navigbar';
    import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
    import Home from './pages';
    import About from './pages/about';
    import Contact from './pages/contact';
    import Menu from './pages/menu';
    import Cart from "./pages/cart";
    import Checkout from "./pages/checkout";
    

    function Applic() {

    return (
       
    <Router>
        <Navbar />
        <Routes>
            <Route path='/website'  element={<Home />} />
            <Route path='/'  element={<Home />} />
            <Route path='/about' element={<About/>} />
            <Route path='/menu' element={<Menu/>} />
            <Route path='/contact' element={<Contact/>} />
            <Route path='/cart' element={<Cart/>} />
            <Route path='/checkout' element={<Checkout/>} />

        </Routes>
    </Router>
    
    );
    
    }

    export default Applic;
